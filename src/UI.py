import typing
from PyQt6.QtWidgets import QMainWindow, QWidget
from Data import Server

class MainWindow(QMainWindow):
    def __init__(self) -> None:
        super().__init__()
        

class ServerWidget(QWidget):
    def __init__(self, server: Server, parent: typing.Optional['QWidget'] = ...) -> None:
        super().__init__(parent)
        self.server = server